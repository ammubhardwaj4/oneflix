import React from 'react'
import { Box, Container } from '@mui/material'
import Button from '@mui/material/Button';
import LoginIcon from '@mui/icons-material/Login';
import { makeStyles } from '@mui/styles';
import Typography from '@mui/material/Typography';
import { NavLink } from 'react-router-dom';



const useStyles = makeStyles({
    parent: {
        display: 'grid',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: '2%'

    },
    contents: {
        width: '70rem',
        height: '50rem',
        boxShadow: '0 0 1rem 0 rgba(0, 0, 0, .2)', 
        borderRadius: '5px',
        backgroundColor: 'rgba(255, 255, 255, .15)',
        backdropFilter: 'blur(5px)',
       
    }
  });


function HomeMain() {
    const styles = useStyles();

  return (
        <Container className={styles.parent} >
            <Box className={styles.contents} mt={10}> 
            <Typography variant="h2" gutterBottom component="h1" mt={10} color="white">
                Welcome to OneFlix......
            </Typography>
            <Typography variant="h4"  component="h4" mt={5} color="white">
                Unlimited movies, TV shows and more.
            </Typography>
            <Typography variant="h6" gutterBottom component="h4" mt={3} color="white">
                Watch anywhere. Cancel anytime.
            </Typography>
            <Typography variant="paragraph" gutterBottom component="div" color="white">
            OneFin is a suite of financial technology tools that your business can benefit from. We ourselves have run businesses and we know the amount of time, effort & resources spent in performing financial operations. Be it bulk payouts, salary transfers, collections, reconciling payments & so on - we have technologies that make all this happen at a click of a button so that you can spend more time on what you are best at - running your business. 
            
             OneFin has been built based on first hand experiences of many problems businesses, especially SMEs, face in India. We firmly believe that in order to run a profitable business that can scale, right set of technology tools are a must. Over last 3.5 years, we have facilitated many lenders, fintech companies & SMEs in their growth stories.

            Let's talk about how we can help you with your growth & financial technology plans. 
            </Typography>
            <Typography variant="button" mt={10} display="block" color="white">
                Click here to login first
            </Typography>

                <Box mt={10}>
                <NavLink to={'/login'}  style={{textDecoration: 'none'}}>
                    <Button variant="outlined" color="error" size='large' endIcon={<LoginIcon />}>
                        Sign In
                    </Button>
                </NavLink>
                </Box>
            </Box>
        </Container>
  )
}

export default HomeMain