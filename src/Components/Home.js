import React from "react";
import { Box } from "@mui/material";
import Header from "./Header";
import HomeMain from "./HomeMain";

function Home() {
  return (
    <Box>
      <Header />
      <HomeMain />
    </Box>
  );
}

export default Home;
