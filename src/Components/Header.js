import React from "react";
import { NavLink } from "react-router-dom";
import { useNavigate } from "react-router-dom";

import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import Tooltip from "@mui/material/Tooltip";
import LoginIcon from "@mui/icons-material/Login";
import { makeStyles } from "@mui/styles";
import logo from "../images/oneFine-logo.png";
import avatarImg from "../images/avatar.webp";

const useStyles = makeStyles({
  root: {
    backgroundColor: "transparent !important",
    boxShadow: "inset 0 0 2000px rgba(255, 255, 255, .5)",
    color: (props) => props.color,
  },
});

function Header(props) {
  const classes = useStyles(props);
  let token = localStorage.getItem("token");
  const navigate = useNavigate();

  const handleLogout = (e) => {
    e.preventDefault();
    localStorage.removeItem("token");
    navigate("/");
  };

  return (
    <AppBar position="fixed" className={classes.root}>
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <NavLink to={"/login"} style={{ textDecoration: "none" }}>
            <Typography
              variant="h6"
              noWrap
              component="div"
              sx={{ mr: 2, display: { xs: "none", md: "flex" } }}
            >
              <img src={logo} alt="Logo" />
            </Typography>
          </NavLink>
          <Box
            sx={{
              display: "flex",
              justifyContent: "flex-end",
              flexGrow:1
            }}
          >
            {token ? (
              <Box>
                <NavLink to={"/dashboard"} style={{ textDecoration: "none" }}>
                <Tooltip title="Hello! User">
                  <IconButton sx={{ pl: 3 }}>
                    <Avatar alt="Test User" src={avatarImg} />
                  </IconButton>
                </Tooltip>
                </NavLink>
                <Button
                  variant="outlined"
                  color="success"
                  onClick={handleLogout}
                  endIcon={<LoginIcon />}
                >
                  Log Out
                </Button>
              </Box>
            ) : (
              <Box>
                <NavLink to={"/login"} style={{ textDecoration: "none" }}>
                  <Button
                    variant="contained"
                    color="error"
                    size="large"
                    endIcon={<LoginIcon />}
                  >
                    Sign In
                  </Button>
                </NavLink>
              </Box>
            )}
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
}

export default Header;
