import React, { useEffect, useState } from "react";
import { makeStyles } from "@mui/styles";
import { NavLink } from "react-router-dom";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import TextField from "@mui/material/TextField";
import AccountCircle from "@mui/icons-material/AccountCircle";
import LockIcon from "@mui/icons-material/Lock";
import Button from "@mui/material/Button";
import LoginIcon from "@mui/icons-material/Login";
import Divider from "@mui/material/Divider";
import HomeIcon from "@mui/icons-material/Home";
import axios from "axios";
import get from "lodash/get";
import Snackbar from "@mui/material/Snackbar";
import Alert from "@mui/material/Alert";
import { useNavigate } from "react-router-dom";

const useStyles = makeStyles({
  headline: {
    fontWeight: "bolder",
    background: "-webkit-linear-gradient(45deg, #302f45 30%, #80e826 90%)",
    WebkitBackgroundClip: "text",
    WebkitTextFillColor: "transparent",
    paddingTop: "30px"
  },
  loginBg: {
    width: "70rem",
    height: "50rem",
    boxShadow: "0 0 1rem 0 rgba(0, 0, 0, .2)",
    borderRadius: "5px",
    background:
      "linear-gradient(217deg, rgba(255,0,0,.8), rgba(255,0,0,0) 70.71%), linear-gradient(127deg, rgba(0,255,0,.8), rgba(0,255,0,0) 70.71%), linear-gradient(336deg, rgba(0,0,255,.8), rgba(0,0,255,0) 70.71%)",
    backdropFilter: "blur(10px)"
  }
});

function Login(props) {
  const classes = useStyles();
  const navigate = useNavigate();
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [showSnackBar, setShowSnackBar] = useState(false);
  const [snackBarMessage, setSnackBarMessage] = useState("");
  const [disableSubmit, setDisableSubmit] = useState(false);
  let token = localStorage.getItem("token");

  useEffect(() => {
    console.log("UER", token);
    if (token) {
      navigate("/dashboard");
      return null;
    }
  }, []);

  const handleUserNameOnChange = (e) => {
    setUserName(e.target.value);
  };

  const handlePasswordOnChange = (e) => {
    setPassword(e.target.value);
  };

  const handleLoginClick = async () => {
    if (userName && password) {
      try {
        setDisableSubmit(true);
        const { data = {} } = await axios({
          method: "post",
          url: "https://demo.credy.in/api/v1/usermodule/login/",
          data: {
            username: userName,
            password
          }
        });
        const token = get(data, "data.token", null);
        localStorage.setItem("token", token);
        console.log("Data", data, token, localStorage.getItem("token"));
        setDisableSubmit(false);
        navigate("/dashboard");
      } catch (error) {
        console.error(error);
        setShowSnackBar(true);
        setDisableSubmit(false);
        setSnackBarMessage(error.message);
      }
    } else {
      setShowSnackBar(true);
      setSnackBarMessage("Please provide mandatory field!");
    }
  };

  const handleClose = () => {
    setShowSnackBar(false);
  };

  console.log(" Login", props);

  return (
    <>
      <Container className={classes.loginBg} maxWidth="sm">
        <Box>
          <Typography
            className={classes.headline}
            variant="h3"
            gutterBottom
            component="h3"
            mt={10}
          >
            Welcome to Sign In
          </Typography>
          <Typography variant="h4" component="h4" mt={5} color="white">
            Unlimited movies, TV shows and more.
          </Typography>

          <Divider variant="inset" component="li" />

          <Box
            sx={{
              display: "flex",
              alignItems: "flex-end",
              justifyContent: "center"
            }}
            mt={15}
          >
            <AccountCircle sx={{ color: "action.active", mr: 1, my: 0.5 }} />
            <TextField
              type="text"
              name="username"
              id="input-with-sx"
              label="UserName"
              variant="standard"
              onChange={handleUserNameOnChange}
              fullWidth
              required
            />
          </Box>
          <Box
            sx={{
              display: "flex",
              alignItems: "flex-end",
              justifyContent: "center"
            }}
            mt={5}
          >
            <LockIcon sx={{ color: "action.active", mr: 1, my: 0.5 }} />
            <TextField
              type="password"
              name="password"
              id="input-with-sx"
              label="Password"
              variant="standard"
              onChange={handlePasswordOnChange}
              fullWidth
              required
            />
          </Box>
          <Box mt={10}>
            <Button
              type="submit"
              variant="contained"
              color="success"
              size="large"
              endIcon={<LoginIcon />}
              onClick={handleLoginClick}
              disabled={disableSubmit}
            >
              Log In
            </Button>
          </Box>
          <Typography
            variant="subtitle2"
            component="div"
            mt={5}
            color="white"
            sx={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            Back to Home Page.......
            <NavLink to={"/"} style={{ textDecoration: "none" }}>
              <Button size="medium" color="warning" endIcon={<HomeIcon />}>
                Click Here
              </Button>
            </NavLink>
          </Typography>
        </Box>
      </Container>
      <Snackbar
        open={showSnackBar}
        autoHideDuration={6000}
        onClose={handleClose}
      >
        <Alert onClose={handleClose} severity={"error"}>
          {snackBarMessage}
        </Alert>
      </Snackbar>
    </>
  );
}

export default Login;
