import React, { useState, useEffect } from "react";
import { Box, Container, Pagination } from "@mui/material";
import { styled, alpha } from "@mui/material/styles";
import InputBase from "@mui/material/InputBase";
import SearchIcon from "@mui/icons-material/Search";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
// import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
// import { Button, CardActionArea, CardActions } from "@mui/material";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import get from "lodash/get";
import CardHeader from "@mui/material/CardHeader";
import Avatar from "@mui/material/Avatar";
import Grid from "@mui/material/Grid";
import Snackbar from "@mui/material/Snackbar";
import Alert from "@mui/material/Alert";
import Chip from "@mui/material/Chip";

import Header from "./Header";

import "./Dashboard.css";

const Search = styled("div")(({ theme }) => ({
  position: "relative",
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  "&:hover": {
    backgroundColor: alpha(theme.palette.common.white, 0.25)
  },
  marginLeft: 0,
  width: "50%",
  [theme.breakpoints.up("sm")]: {
    marginLeft: theme.spacing(1),
    width: "auto"
  }
}));

const SearchIconWrapper = styled("div")(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: "100%",
  position: "absolute",
  pointerEvents: "none",
  display: "flex",
  alignItems: "center",
  justifyContent: "center"
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: "inherit",
  "& .MuiInputBase-input": {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      width: "12ch",
      "&:focus": {
        width: "20ch"
      }
    }
  }
}));
let dta=null;
function Dashboard() {
  let token = localStorage.getItem("token");
  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);
  const [searchInput, setSearchInput] = useState("");
  const [movieList, setMovieList] = useState([]);
  const [showSnackBar, setShowSnackBar] = useState(false);
  const [snackBarMessage, setSnackBarMessage] = useState("");
  const [count, setCount] = useState(0);
  const [page, setPage] = React.useState(1);
  let url = `https://demo.credy.in/api/v1/maya/movies/?page=${page}`;

  useEffect(() => {
    console.log("UE", token);
    if (!token) {
      navigate("/login");
      return null;
    } else {
      fetchMovies(url);
    }
  }, []);

  const fetchMovies = async (fetchUrl) => {
    try {
      setLoading(true);
      const { data = {} } = await axios.get(fetchUrl, {
        headers: {
          Authorization: `Token ${token}`
        }
      });
      setLoading(false);
      setMovieList(get(data, "results", []));
      setCount(Math.ceil(get(data, "count", 0) / 10));
      console.log("Data", data);
    } catch (error) {
      console.error(error);
      setLoading(false);
      setShowSnackBar(true);
      setSnackBarMessage(error.message);
    }
  };

  const handleClose = () => {
    setShowSnackBar(false);
  };

  const handleChange = (event, value) => {
    setPage(value);
    fetchMovies(`https://demo.credy.in/api/v1/maya/movies/?page=${value}`);
  };

  const handleSearch = (e) => {
    e.preventDefault();
    setSearchInput(e.target.value);
  }

  if(searchInput.length > 0) {
   dta = movieList.filter((i) => {
      console.log(dta, i.title.match(searchInput));
    })
  }

  // console.log("Movielist", movieList, next, prev, count, page);

  return (
    <Box>
      <Header />
      <Box className="dashboardContent">
      <Box mb={2} pt={10} sx={{ display: "flex", justifyContent: "center" }}>
        <Search>
          <SearchIconWrapper>
            <SearchIcon />
          </SearchIconWrapper>
          <StyledInputBase
            placeholder="Search…"
            inputProps={{ "aria-label": "search" }}
            value={searchInput}
            onChange={handleSearch}
          />
        </Search>
      </Box>

      <Box p={2}>
      <Box sx={{ bgcolor: "#cfe8fc", height: "90vh" }}>
        
        <Grid container spacing={2} sx={{ display: "flex" }}>
          {movieList.map((item, index) => {
            const title = get(item, "title", "");
            const description = get(item, "description", "");
            const genres = get(item, "genres", "");
            return (
              <Grid item sm={3} md={3} lg={3} key={item.uuid}>
                <Card>
                  <CardHeader
                    avatar={
                      <Avatar
                        src={`https://ui-avatars.com/api/?name=${title}&background=0D8ABC&color=fff`}
                      />
                    }
                    title={title}
                  />
                  {description && (
                    <CardContent className={"cardContainer"}>
                      <Typography
                        component={"p"}
                        variant="body2"
                        color="text.secondary"
                        className={"description"}
                      >
                        {description.substr(0, 200)}
                      </Typography>
                      {genres && (
                        <Grid pt={2} container spacing={1} sx={{display:'flex', justifyContent:'center'}} >
                          {genres.split(",").map((value, key) => {
                            return (
                              <Grid key={value} item sm={3} md={3} lg={3}>
                                <Chip
                                  label={value}
                                  color="primary"
                                  variant="outlined"
                                  className={"chipStyle"}
                                />
                              </Grid>
                            );
                          })}
                        </Grid>
                      )}
                    </CardContent>
                  )}
                </Card>
              </Grid>
            );
          })}
        </Grid>

        <Box
          mt={2}
          sx={{
            display: "flex",
            justifyContent: "flex-end",
            alignItems: "center",
            alignSelf: "flex-end"
          }}
        >
          <Pagination
            count={count}
            page={page}
            variant="outlined"
            color="primary"
            onChange={handleChange}
          />
        </Box>
        </Box>
      </Box>
      <Snackbar
        open={showSnackBar}
        autoHideDuration={6000}
        onClose={handleClose}
      >
        <Alert onClose={handleClose} severity={"error"}>
          {snackBarMessage}
        </Alert>
      </Snackbar>
      </Box>
    </Box>
  );
}

export default Dashboard;
